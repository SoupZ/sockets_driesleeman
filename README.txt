

- Server - Client connection via TCP / IP
- Server uses a child process for each connection with "fork"
- Client connects and sends file to  server, data is stored by server in a file.
- The client receives the file  via a command-line parameter.
- The server creates a file with a date in the file name
